//Sets pins globally
//c code
int redPin = 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
int n = 0;
int count;

void loop() {
  setColor(255,0,0);
  delay(500);
  setColor(0,0,255);
  delay(500);
  setColor(0,255,0);
  delay(500);

}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
